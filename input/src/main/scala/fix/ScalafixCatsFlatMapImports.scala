/*
rule = ScalafixCatsImports
 */
package fix

import cats.Monad
import cats.implicits._

class ScalafixCatsFlatMapImports[F[_]: Monad, A, B](f: A => F[B], fa: A => B) {

  val r = f.flatMap(fb => a => fa(a))
}
