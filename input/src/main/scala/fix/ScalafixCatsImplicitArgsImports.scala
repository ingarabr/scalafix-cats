/*
rule = ScalafixCatsImports
 */
package fix

import cats.data.NonEmptyList
import cats.implicits._

object ScalafixCatsImplicitArgsImports {
  val str: String = NonEmptyList.one(1).mkString_(", ")
}
