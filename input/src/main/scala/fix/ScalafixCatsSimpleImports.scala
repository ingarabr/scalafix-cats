/*
rule = ScalafixCatsImports
 */
package fix

import cats.Monad
import cats.implicits._

object ScalafixCatsSimpleImports {
  val a: String = "a"
  val b: BigInt = BigInt(200)
  show"$a $b"

  def foo[F[_]: Monad]: F[String] =
    for {
      r <- "en".pure[F]
      _ <- "to".pure[F]
    } yield r
}
