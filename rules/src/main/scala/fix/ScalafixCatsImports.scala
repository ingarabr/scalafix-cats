package fix

import scalafix.v1._

import scala.collection.immutable.SortedSet
import scala.meta._

class ScalafixCatsImports extends SemanticRule("ScalafixCatsImports") {
  override def fix(implicit doc: SemanticDocument): Patch = {
    doc.tree.collect {
      case i @ Importer(
            Term.Select(Term.Name("cats"), Term.Name("implicits")),
            List(Importee.Wildcard())
          ) =>
        val fromImplicits: List[String] = doc.synthetics.toList.flatMap(findImplicits)

        val fromTerms: List[String] =
          doc.tree.collect {
            case t: Term => t.symbol.value
          }

        val newImports: SortedSet[String] =
          SortedSet.empty[String] ++ fromImplicits ++ fromTerms flatMap {
            case path `#` _ =>
              Mappings get path
            case _ => None
          }

        val removeWildcardImportPatch = i.importees.map(Patch.removeImportee).asPatch
        val newImportPatch            = Patch.addRight(i.parent.get, newImports.map(i => s"import $i._").mkString("\n", "\n", ""))

        removeWildcardImportPatch + newImportPatch
    }.asPatch
  }

  private def findImplicits(tree: SemanticTree): List[String] = {
    tree match {
      case IdTree(info) =>
        if (info.isImplicit) List(info.symbol.value) else Nil
      case SelectTree(qualifier, id) =>
        findImplicits(qualifier) ++ findImplicits(id)
      case ApplyTree(function, arguments) =>
        findImplicits(function) ++ arguments.flatMap(findImplicits)
      case TypeApplyTree(function, _) =>
        findImplicits(function)
      case FunctionTree(parameters, body) =>
        (parameters flatMap findImplicits) ++ findImplicits(body)
      case LiteralTree(_) =>
        Nil
      case MacroExpansionTree(beforeExpansion, _) =>
        findImplicits(beforeExpansion)
      case OriginalSubTree(_) =>
        Nil
      case OriginalTree(_) =>
        Nil
      case NoTree =>
        Nil
    }
  }

  object `#` {
    def unapply(str: String): Option[(Path, String)] =
      str.indexOf('#') match {
        case -1 => None
        case n  => Some((Path(str.take(n)), str.drop(n + 1)))
      }
  }

  /* This exists to ignore differences between `.` and `/` in paths */
  case class Path(segments: List[String])
  object Path {
    def apply(str: String) = new Path(str.split("[./]").toList)
  }

  val Mappings: Map[Path, String] =
    Map(
      Path("cats.Bifoldable.ToBifoldableOps")                        -> "cats.syntax.bifoldable",
      Path("cats.Bifunctor.ToBifunctorOps")                          -> "cats.syntax.bifunctor",
      Path("cats.CoflatMap.ToCoflatMapOps")                          -> "cats.syntax.coflatMap",
      Path("cats.Comonad.ToComonadOps")                              -> "cats.syntax.comonad",
      Path("cats.Contravariant.ToContravariantOps")                  -> "cats.syntax.contravariant",
      Path("cats.Distributive.ToDistributiveOps")                    -> "cats.syntax.distributive",
      Path("cats.FlatMap.ToFlatMapOps")                              -> "cats.syntax.flatMap",
      Path("cats.Foldable.ToFoldableOps")                            -> "cats.syntax.foldable",
      Path("cats.Functor.ToFunctorOps")                              -> "cats.syntax.functor",
      Path("cats.FunctorFilter.ToFunctorFilterOps")                  -> "cats.syntax.functorFilter",
      Path("cats.Invariant.ToInvariantOps")                          -> "cats.syntax.invariant",
      Path("cats.NonEmptyTraverse.ToNonEmptyTraverseOps")            -> "cats.syntax.nonEmptyTraverse",
      Path("cats.Reducible.ToReducibleOps")                          -> "cats.syntax.reducible",
      Path("cats.SemigroupK.ToSemigroupKOps")                        -> "cats.syntax.semigroupk",
      Path("cats.Show.ToShowOps")                                    -> "cats.syntax.show",
      Path("cats.Traverse.ToTraverseOps")                            -> "cats.syntax.traverse",
      Path("cats.TraverseFilter.ToTraverseFilterOps")                -> "cats.syntax.traverseFilter",
      Path("cats.UnorderedFoldable.ToUnorderedFoldableOps")          -> "cats.syntax.foldable",
      Path("cats.UnorderedFoldable.ToUnorderedFoldableOps")          -> "cats.syntax.unorderedFoldable",
      Path("cats.UnorderedTraverse.ToUnorderedTraverseOps")          -> "cats.syntax.unorderedTraverse",
      Path("cats.arrow.Arrow.ToArrowOps")                            -> "cats.syntax.arrow",
      Path("cats.arrow.ArrowChoice.ToArrowChoiceOps")                -> "cats.syntax.arrowChoice",
      Path("cats.arrow.Choice.ToChoiceOps")                          -> "cats.syntax.choice",
      Path("cats.arrow.Compose.ToComposeOps")                        -> "cats.syntax.compose",
      Path("cats.arrow.Profunctor.ToProfunctorOps")                  -> "cats.syntax.profunctor",
      Path("cats.arrow.Strong.ToStrongOps")                          -> "cats.syntax.strong",
      Path("cats.instances.BigDecimalInstances")                     -> "cats.instances.bigDecimal",
      Path("cats.instances.BigIntInstances")                         -> "cats.instances.bigInt",
      Path("cats.instances.BitSetInstances")                         -> "cats.instances.bitSet",
      Path("cats.instances.BooleanInstances")                        -> "cats.instances.boolean",
      Path("cats.instances.ByteInstances")                           -> "cats.instances.byte",
      Path("cats.instances.CharInstances")                           -> "cats.instances.char",
      Path("cats.instances.CoreDurationInstances")                   -> "cats.instances.duration",
      Path("cats.instances.CoreDurationInstances")                   -> "cats.instances.finiteDuration",
      Path("cats.instances.CoreFiniteDurationInstances")             -> "cats.instances.finiteDuration",
      Path("cats.instances.DoubleInstances")                         -> "cats.instances.double",
      Path("cats.instances.DurationInstances")                       -> "cats.instances.duration",
      Path("cats.instances.EitherInstances")                         -> "cats.instances.either",
      Path("cats.instances.EqInstances")                             -> "cats.instances.eq",
      Path("cats.instances.EquivInstances")                          -> "cats.instances.equiv",
      Path("cats.instances.FiniteDurationInstances")                 -> "cats.instances.finiteDuration",
      Path("cats.instances.FloatInstances")                          -> "cats.instances.float",
      Path("cats.instances.Function0Instances")                      -> "cats.instances.function",
      Path("cats.instances.Function0Instances0")                     -> "cats.instances.function",
      Path("cats.instances.Function1Instances")                      -> "cats.instances.function",
      Path("cats.instances.Function1Instances0")                     -> "cats.instances.function",
      Path("cats.instances.FunctionInstances")                       -> "cats.instances.function",
      Path("cats.instances.FunctionInstancesBinCompat0")             -> "cats.instances.function",
      Path("cats.instances.FutureInstances")                         -> "cats.instances.future",
      Path("cats.instances.FutureInstances1")                        -> "cats.instances.future",
      Path("cats.instances.FutureInstances2")                        -> "cats.instances.future",
      Path("cats.instances.IntInstances")                            -> "cats.instances.int",
      Path("cats.instances.InvariantMonoidalInstances")              -> "cats.instances.invariant",
      Path("cats.instances.ListInstances")                           -> "cats.instances.list",
      Path("cats.instances.ListInstancesBinCompat0")                 -> "cats.instances.list",
      Path("cats.instances.LongInstances")                           -> "cats.instances.long",
      Path("cats.instances.LowPrioritySortedSetInstancesBinCompat1") -> "cats.instances.sortedSet",
      Path("cats.instances.MapInstances")                            -> "cats.instances.map",
      Path("cats.instances.MapInstancesBinCompat0")                  -> "cats.instances.map",
      Path("cats.instances.MapInstancesBinCompat1")                  -> "cats.instances.map",
      Path("cats.instances.OptionInstances")                         -> "cats.instances.option",
      Path("cats.instances.OptionInstancesBinCompat0")               -> "cats.instances.option",
      Path("cats.instances.OrderInstances")                          -> "cats.instances.order",
      Path("cats.instances.OrderingInstances")                       -> "cats.instances.ordering",
      Path("cats.instances.ParallelInstances")                       -> "cats.instances.parallel",
      Path("cats.instances.ParallelInstances1")                      -> "cats.instances.parallel",
      Path("cats.instances.PartialOrderInstances")                   -> "cats.instances.partialOrder",
      Path("cats.instances.PartialOrderingInstances")                -> "cats.instances.partialOrdering",
      Path("cats.instances.QueueInstances")                          -> "cats.instances.queue",
      Path("cats.instances.SetInstances")                            -> "cats.instances.set",
      Path("cats.instances.ShortInstances")                          -> "cats.instances.short",
      Path("cats.instances.SortedMapInstances")                      -> "cats.instances.sortedMap",
      Path("cats.instances.SortedMapInstances1")                     -> "cats.instances.sortedMap",
      Path("cats.instances.SortedMapInstances2")                     -> "cats.instances.sortedMap",
      Path("cats.instances.SortedMapInstancesBinCompat0")            -> "cats.instances.sortedMap",
      Path("cats.instances.SortedMapInstancesBinCompat1")            -> "cats.instances.sortedMap",
      Path("cats.instances.SortedMapInstancesBinCompat2")            -> "cats.instances.sortedMap",
      Path("cats.instances.SortedSetInstances")                      -> "cats.instances.sortedSet",
      Path("cats.instances.SortedSetInstances1")                     -> "cats.instances.sortedSet",
      Path("cats.instances.SortedSetInstancesBinCompat0")            -> "cats.instances.sortedSet",
      Path("cats.instances.SortedSetInstancesBinCompat1")            -> "cats.instances.sortedSet",
      Path("cats.instances.StreamInstances")                         -> "cats.instances.stream",
      Path("cats.instances.StreamInstancesBinCompat0")               -> "cats.instances.stream",
      Path("cats.instances.StringInstances")                         -> "cats.instances.string",
      Path("cats.instances.TryInstances")                            -> "cats.instances.try_",
      Path("cats.instances.TryInstances1")                           -> "cats.instances.try_",
      Path("cats.instances.TryInstances2")                           -> "cats.instances.try_",
      Path("cats.instances.Tuple2Instances")                         -> "cats.instances.tuple",
      Path("cats.instances.Tuple2Instances1")                        -> "cats.instances.tuple",
      Path("cats.instances.Tuple2Instances2")                        -> "cats.instances.tuple",
      Path("cats.instances.Tuple2Instances3")                        -> "cats.instances.tuple",
      Path("cats.instances.Tuple2Instances4")                        -> "cats.instances.tuple",
      Path("cats.instances.Tuple2InstancesBinCompat0")               -> "cats.instances.tuple",
      Path("cats.instances.TupleInstances")                          -> "cats.instances.tuple",
      Path("cats.instances.UUIDInstances")                           -> "cats.instances.uuid",
      Path("cats.instances.UnitInstances")                           -> "cats.instances.unit",
      Path("cats.instances.VectorInstances")                         -> "cats.instances.vector",
      Path("cats.instances.VectorInstancesBinCompat0")               -> "cats.instances.vector",
      Path("cats.kernel.EqToEquivConversion")                        -> "cats.instances.eq",
      Path("cats.kernel.OrderToOrderingConversion")                  -> "cats.instances.order",
      Path("cats.kernel.PartialOrderToPartialOrderingConversion")    -> "cats.instances.partialOrder",
      Path("cats.kernel.instances.BigDecimalInstances")              -> "cats.instances.bigDecimal",
      Path("cats.kernel.instances.BigIntInstances")                  -> "cats.instances.bigInt",
      Path("cats.kernel.instances.BitSetInstances")                  -> "cats.instances.bitSet",
      Path("cats.kernel.instances.BooleanInstances")                 -> "cats.instances.boolean",
      Path("cats.kernel.instances.ByteInstances")                    -> "cats.instances.byte",
      Path("cats.kernel.instances.CharInstances")                    -> "cats.instances.char",
      Path("cats.kernel.instances.DoubleInstances")                  -> "cats.instances.double",
      Path("cats.kernel.instances.DurationInstances")                -> "cats.instances.duration",
      Path("cats.kernel.instances.EitherInstances")                  -> "cats.instances.either",
      Path("cats.kernel.instances.EitherInstances0")                 -> "cats.instances.either",
      Path("cats.kernel.instances.EitherInstances1")                 -> "cats.instances.either",
      Path("cats.kernel.instances.EqInstances")                      -> "cats.instances.eq",
      Path("cats.kernel.instances.FiniteDurationInstances")          -> "cats.instances.finiteDuration",
      Path("cats.kernel.instances.FloatInstances")                   -> "cats.instances.float",
      Path("cats.kernel.instances.FunctionInstances")                -> "cats.instances.function",
      Path("cats.kernel.instances.FunctionInstances0")               -> "cats.instances.function",
      Path("cats.kernel.instances.FunctionInstances1")               -> "cats.instances.function",
      Path("cats.kernel.instances.FunctionInstances2")               -> "cats.instances.function",
      Path("cats.kernel.instances.FunctionInstances3")               -> "cats.instances.function",
      Path("cats.kernel.instances.FunctionInstances4")               -> "cats.instances.function",
      Path("cats.kernel.instances.IntInstances")                     -> "cats.instances.int",
      Path("cats.kernel.instances.ListInstances")                    -> "cats.instances.list",
      Path("cats.kernel.instances.ListInstances1")                   -> "cats.instances.list",
      Path("cats.kernel.instances.ListInstances2")                   -> "cats.instances.list",
      Path("cats.kernel.instances.LongInstances")                    -> "cats.instances.long",
      Path("cats.kernel.instances.MapInstances")                     -> "cats.instances.map",
      Path("cats.kernel.instances.MapInstances1")                    -> "cats.instances.map",
      Path("cats.kernel.instances.OptionInstances")                  -> "cats.instances.option",
      Path("cats.kernel.instances.OptionInstances0")                 -> "cats.instances.option",
      Path("cats.kernel.instances.OptionInstances1")                 -> "cats.instances.option",
      Path("cats.kernel.instances.OptionInstances2")                 -> "cats.instances.option",
      Path("cats.kernel.instances.OrderInstances")                   -> "cats.instances.order",
      Path("cats.kernel.instances.PartialOrderInstances")            -> "cats.instances.partialOrder",
      Path("cats.kernel.instances.QueueInstances")                   -> "cats.instances.queue",
      Path("cats.kernel.instances.QueueInstances1")                  -> "cats.instances.queue",
      Path("cats.kernel.instances.QueueInstances2")                  -> "cats.instances.queue",
      Path("cats.kernel.instances.SetInstances")                     -> "cats.instances.set",
      Path("cats.kernel.instances.SetInstances1")                    -> "cats.instances.set",
      Path("cats.kernel.instances.ShortInstances")                   -> "cats.instances.short",
      Path("cats.kernel.instances.SortedMapInstances")               -> "cats.instances.sortedMap",
      Path("cats.kernel.instances.SortedMapInstances1")              -> "cats.instances.sortedMap",
      Path("cats.kernel.instances.SortedMapInstances2")              -> "cats.instances.sortedMap",
      Path("cats.kernel.instances.SortedSetInstances")               -> "cats.instances.sortedSet",
      Path("cats.kernel.instances.SortedSetInstances1")              -> "cats.instances.sortedSet",
      Path("cats.kernel.instances.StreamInstances")                  -> "cats.instances.stream",
      Path("cats.kernel.instances.StreamInstances1")                 -> "cats.instances.stream",
      Path("cats.kernel.instances.StreamInstances2")                 -> "cats.instances.stream",
      Path("cats.kernel.instances.StringInstances")                  -> "cats.instances.string",
      Path("cats.kernel.instances.TupleInstances")                   -> "cats.instances.tuple",
      Path("cats.kernel.instances.TupleInstances1")                  -> "cats.instances.tuple",
      Path("cats.kernel.instances.TupleInstances2")                  -> "cats.instances.tuple",
      Path("cats.kernel.instances.TupleInstances3")                  -> "cats.instances.tuple",
      Path("cats.kernel.instances.UUIDInstances")                    -> "cats.instances.uuid",
      Path("cats.kernel.instances.UnitInstances")                    -> "cats.instances.unit",
      Path("cats.kernel.instances.VectorInstances")                  -> "cats.instances.vector",
      Path("cats.kernel.instances.VectorInstances1")                 -> "cats.instances.vector",
      Path("cats.kernel.instances.VectorInstances2")                 -> "cats.instances.vector",
      Path("cats.syntax.AlternativeSyntax")                          -> "cats.syntax.alternative",
      Path("cats.syntax.ApplicativeErrorSyntax")                     -> "cats.syntax.applicativeError",
      Path("cats.syntax.ApplicativeSyntax")                          -> "cats.syntax.applicative",
      Path("cats.syntax.ApplySyntax")                                -> "cats.syntax.apply",
      Path("cats.syntax.ArrowChoiceSyntax")                          -> "cats.syntax.arrowChoice",
      Path("cats.syntax.ArrowSyntax")                                -> "cats.syntax.arrow",
      Path("cats.syntax.BifoldableSyntax")                           -> "cats.syntax.bifoldable",
      Path("cats.syntax.BifunctorSyntax")                            -> "cats.syntax.bifunctor",
      Path("cats.syntax.BinestedSyntax")                             -> "cats.syntax.binested",
      Path("cats.syntax.BitraverseSyntax")                           -> "cats.syntax.bitraverse",
      Path("cats.syntax.BitraverseSyntax1")                          -> "cats.syntax.bitraverse",
      Path("cats.syntax.BitraverseSyntaxBinCompat0")                 -> "cats.syntax.bitraverse",
      Path("cats.syntax.ChoiceSyntax")                               -> "cats.syntax.choice",
      Path("cats.syntax.CoflatMapSyntax")                            -> "cats.syntax.coflatMap",
      Path("cats.syntax.ComonadSyntax")                              -> "cats.syntax.comonad",
      Path("cats.syntax.ComposeSyntax")                              -> "cats.syntax.compose",
      Path("cats.syntax.ContravariantMonoidalSyntax")                -> "cats.syntax.contravariantMonoidal",
      Path("cats.syntax.ContravariantSemigroupalSyntax")             -> "cats.syntax.contravariantSemigroupal",
      Path("cats.syntax.ContravariantSyntax")                        -> "cats.syntax.contravariant",
      Path("cats.syntax.DistributiveSyntax")                         -> "cats.syntax.distributive",
      Path("cats.syntax.EitherKSyntax")                              -> "cats.syntax.eitherK",
      Path("cats.syntax.EitherSyntax")                               -> "cats.syntax.either",
      Path("cats.syntax.EitherSyntaxBinCompat0")                     -> "cats.syntax.either",
      Path("cats.syntax.EqSyntax")                                   -> "cats.syntax.eq",
      Path("cats.syntax.EqSyntax")                                   -> "cats.syntax.order",
      Path("cats.syntax.EqSyntax")                                   -> "cats.syntax.partialOrder",
      Path("cats.syntax.FlatMapSyntax")                              -> "cats.syntax.flatMap",
      Path("cats.syntax.FoldableSyntax")                             -> "cats.syntax.foldable",
      Path("cats.syntax.FoldableSyntaxBinCompat0")                   -> "cats.syntax.foldable",
      Path("cats.syntax.FoldableSyntaxBinCompat1")                   -> "cats.syntax.foldable",
      Path("cats.syntax.FunctorFilterSyntax")                        -> "cats.syntax.functorFilter",
      Path("cats.syntax.FunctorSyntax")                              -> "cats.syntax.functor",
      Path("cats.syntax.GroupSyntax")                                -> "cats.syntax.group",
      Path("cats.syntax.InvariantSyntax")                            -> "cats.syntax.invariant",
      Path("cats.syntax.IorSyntax")                                  -> "cats.syntax.ior",
      Path("cats.syntax.ListSyntax")                                 -> "cats.syntax.list",
      Path("cats.syntax.ListSyntaxBinCompat0")                       -> "cats.syntax.list",
      Path("cats.syntax.MonadErrorSyntax")                           -> "cats.syntax.monadError",
      Path("cats.syntax.MonadSyntax")                                -> "cats.syntax.monad",
      Path("cats.syntax.MonoidSyntax")                               -> "cats.syntax.monoid",
      Path("cats.syntax.NestedSyntax")                               -> "cats.syntax.nested",
      Path("cats.syntax.NonEmptyTraverseSyntax")                     -> "cats.syntax.nonEmptyTraverse",
      Path("cats.syntax.OptionSyntax")                               -> "cats.syntax.option",
      Path("cats.syntax.OrderSyntax")                                -> "cats.syntax.order",
      Path("cats.syntax.ParallelApplySyntax")                        -> "cats.syntax.parallel",
      Path("cats.syntax.ParallelBitraverseSyntax")                   -> "cats.syntax.parallel",
      Path("cats.syntax.ParallelFlatSyntax")                         -> "cats.syntax.parallel",
      Path("cats.syntax.ParallelSyntax")                             -> "cats.syntax.parallel",
      Path("cats.syntax.ParallelTraverseSyntax")                     -> "cats.syntax.parallel",
      Path("cats.syntax.ParallelUnorderedTraverseSyntax")            -> "cats.syntax.parallel",
      Path("cats.syntax.PartialOrderSyntax")                         -> "cats.syntax.order",
      Path("cats.syntax.PartialOrderSyntax")                         -> "cats.syntax.partialOrder",
      Path("cats.syntax.ProfunctorSyntax")                           -> "cats.syntax.profunctor",
      Path("cats.syntax.ReducibleSyntax")                            -> "cats.syntax.reducible",
      Path("cats.syntax.ReducibleSyntaxBinCompat0")                  -> "cats.syntax.reducible",
      Path("cats.syntax.RepresentableSyntax")                        -> "cats.syntax.representable",
      Path("cats.syntax.SemigroupKSyntax")                           -> "cats.syntax.semigroupk",
      Path("cats.syntax.SemigroupSyntax")                            -> "cats.syntax.group",
      Path("cats.syntax.SemigroupSyntax")                            -> "cats.syntax.monoid",
      Path("cats.syntax.SemigroupSyntax")                            -> "cats.syntax.semigroup",
      Path("cats.syntax.SemigroupalSyntax")                          -> "cats.syntax.cartesian",
      Path("cats.syntax.SemigroupalSyntax")                          -> "cats.syntax.semigroupal",
      Path("cats.syntax.SetSyntax")                                  -> "cats.syntax.set",
      Path("cats.syntax.ShowSyntax")                                 -> "cats.syntax.show",
      Path("cats.syntax.StrongSyntax")                               -> "cats.syntax.strong",
      Path("cats.syntax.TraverseFilterSyntax")                       -> "cats.syntax.traverseFilter",
      Path("cats.syntax.TraverseFilterSyntaxBinCompat0")             -> "cats.syntax.traverseFilter",
      Path("cats.syntax.TraverseSyntax")                             -> "cats.syntax.traverse",
      Path("cats.syntax.TrySyntax")                                  -> "cats.syntax.try_",
      Path("cats.syntax.TupleParallelSyntax")                        -> "cats.syntax.parallel",
      Path("cats.syntax.TupleSemigroupalSyntax")                     -> "cats.syntax.apply",
      Path("cats.syntax.TupleSemigroupalSyntax")                     -> "cats.syntax.contravariantSemigroupal",
      Path("cats.syntax.UnorderedFoldableSyntax")                    -> "cats.syntax.unorderedFoldable",
      Path("cats.syntax.UnorderedTraverseSyntax")                    -> "cats.syntax.unorderedTraverse",
      Path("cats.syntax.ValidatedExtensionSyntax")                   -> "cats.syntax.validated",
      Path("cats.syntax.ValidatedSyntax")                            -> "cats.syntax.validated",
      Path("cats.syntax.ValidatedSyntaxBincompat0")                  -> "cats.syntax.validated",
      Path("cats.syntax.VectorSyntax")                               -> "cats.syntax.vector",
      Path("cats.syntax.WriterSyntax")                               -> "cats.syntax.writer"
    )
}
