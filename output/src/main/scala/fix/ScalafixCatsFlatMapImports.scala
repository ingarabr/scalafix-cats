package fix

import cats.Monad
import cats.instances.function._
import cats.syntax.flatMap._

class ScalafixCatsFlatMapImports[F[_]: Monad, A, B](f: A => F[B], fa: A => B) {

  val r = f.flatMap(fb => a => fa(a))
}
