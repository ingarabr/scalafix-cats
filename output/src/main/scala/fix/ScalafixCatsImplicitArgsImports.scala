package fix

import cats.data.NonEmptyList
import cats.instances.int._
import cats.syntax.foldable._

object ScalafixCatsImplicitArgsImports {
  val str: String = NonEmptyList.one(1).mkString_(", ")
}
