package fix

import cats.Semigroup
import cats.data.NonEmptyList
import cats.instances.either._
import cats.instances.int._

object ScalafixCatsSemigroupImports {
  type ErrorOr[A] = Either[String, A]

  private val a: ErrorOr[Int]         = Right(1)
  private val res: ErrorOr[Int]       = NonEmptyList.one(a).reduce
  private val a1: Either[String, Int] = Right(1)

  private val res1: Either[String, Int] = NonEmptyList.one[Either[String, Int]](a).reduce
}
