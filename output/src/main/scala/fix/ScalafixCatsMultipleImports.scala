package fix

import cats.ApplicativeError
import cats.data.{NonEmptyList, Validated, ValidatedNec}
import cats.instances.either._
import cats.instances.int._
import cats.instances.string._
import cats.syntax.foldable._
import cats.syntax.partialOrder._
import cats.syntax.validated._

object ScalafixCatsMultipleImports {
  type ErrorOr[A] = Either[String, A]

  def divide[F[_]](dividend: Int, divisor: Int)(implicit F: ApplicativeError[F, String]): F[Int] =
    if (divisor === 0) F.raiseError("division by zero")
    else F.pure(dividend / divisor)

  private val divided1: ErrorOr[Int] = divide[ErrorOr](6, 3)
  private val divided2: ErrorOr[Int] = divide[ErrorOr](6, 0)

  def fromValidated[F[_]](vNec: ValidatedNec[String, Int])(
      implicit F: ApplicativeError[F, String]): F[Int] =
    vNec match {
      case Validated.Valid(a) => F.pure(a)
      case Validated.Invalid(e) =>
        val str = e.mkString_(", ")
        F.raiseError(str)
    }

  private val validated1: ErrorOr[Int] = fromValidated[ErrorOr](1.validNec)
  private val validated2: ErrorOr[Int] = fromValidated[ErrorOr]("".invalidNec)

  private val res: ErrorOr[Int] = NonEmptyList.of(divided1, divided2, validated1, validated2).reduce
}
