package fix

import cats.Monad
import cats.instances.bigInt._
import cats.instances.string._
import cats.syntax.applicative._
import cats.syntax.flatMap._
import cats.syntax.functor._
import cats.syntax.show._

object ScalafixCatsSimpleImports {
  val a: String = "a"
  val b: BigInt = BigInt(200)
  show"$a $b"

  def foo[F[_]: Monad]: F[String] =
    for {
      r <- "en".pure[F]
      _ <- "to".pure[F]
    } yield r
}
