import scala.collection.mutable

/**
  * This is not particularly nice or clever, but given a list of all the possible detailed imports we look up all
  *  the traits which the particular import extends. The result of running this script is code that can be pasted in
  *  to `ScalafixCatsImports`
  */
object Tables extends App {
  val SyntaxObjects = List(
    "alternative",
    "applicative",
    "applicativeError",
    "apply",
    "arrow",
    "arrowChoice",
    "bifunctor",
    "bifoldable",
    "binested",
    "bitraverse",
    "cartesian",
    "choice",
    "coflatMap",
    "distributive",
    "eitherK",
    "comonad",
    "compose",
    "contravariant",
    "contravariantSemigroupal",
    "contravariantMonoidal",
    "either",
    "eq",
    "flatMap",
    "foldable",
    "functor",
    "functorFilter",
    "group",
    "invariant",
    "ior",
    "list",
    "monad",
    "monadError",
    "monoid",
    "nested",
    "option",
    "order",
    "parallel",
    "partialOrder",
    "profunctor",
    "reducible",
    "representable",
    "semigroup",
    "semigroupal",
    "semigroupk",
    "show",
    "strong",
    "try_",
    "traverse",
    "traverseFilter",
    "nonEmptyTraverse",
    "unorderedFoldable",
    "unorderedTraverse",
    "validated",
    "vector",
    "writer",
    "set"
  )
  val InstancesObjects = List(
    "bigInt",
    "bigDecimal",
    "bitSet",
    "boolean",
    "byte",
    "char",
    "double",
    "duration",
    "either",
    "eq",
    "equiv",
    "float",
    "finiteDuration",
    "function",
    "future",
    "int",
    "invariant",
    "list",
    "long",
    "option",
    "map",
    "order",
    "ordering",
    "parallel",
    "partialOrder",
    "partialOrdering",
    "queue",
    "set",
    "short",
    "sortedMap",
    "sortedSet",
    "stream",
    "string",
    "try_",
    "tuple",
    "unit",
    "uuid",
    "vector",
  )

  def find(pkgName: String)(name: String) = {
    val classes = mutable.ArrayBuffer[String]()
    def go(cls: Class[_]): Unit = {
      if (cls != null &&
          cls.getCanonicalName.startsWith("cats.") &&
          !classes.contains(cls.getCanonicalName)) {
        if (!cls.getCanonicalName.contains("$")) classes += cls.getCanonicalName
        go(cls.getSuperclass)
        cls.getInterfaces foreach go
      }
    }
    go(Class.forName(s"cats.$pkgName.package$$$name$$"))
    s"cats.$pkgName.$name" -> classes.toList
  }

  val syntaxImports: Map[String, List[String]] =
    SyntaxObjects.map(find("syntax")).toMap

  val instancesImports: Map[String, List[String]] =
    InstancesObjects.map(find("instances")).toMap

  def format(x: Map[String, List[String]]): String = {
    def quote(str: String) = '"' + str + '"'
    x.flatMap { case (i, traits) => traits.map(t => s"Path(${quote(t)}) -> ${quote(i)}") }
      .toList
      .sorted
      .mkString("Map(", ",\n", ")")
  }

  println(format(syntaxImports ++ instancesImports))

}
